from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.corpus import wordnet
import nltk
import sys
from nltk.wsd import lesk



def word_sense(sentence,word):
    word_token = nltk.word_tokenize(sentence)
    print(lesk(word_token, word, 'n'))


if __name__ == "__main__":
    word_sense(sys.argv[1],sys.argv[2])
