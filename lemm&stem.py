from nltk.stem import PorterStemmer, WordNetLemmatizer
from nltk.corpus import wordnet
import nltk
import sys

lemmatizer = WordNetLemmatizer()
stemmer = PorterStemmer()

#dictonary to exclude items

exclude = {
    'The' : True,
    'the' : True,
    'is'  : True,
    'Are' : True,
    'are' : True,
    'am'  : True,
    'Am'  : True,
    'Have': True,
    'have': True,
    'had' : True,
    'Had' : True,
    'Has' : True,
    'has' : True,
    'been': True,
}

excptn = {
    'beautiful' : 'beauty',
    'Beautiful' : 'Beauty',
    'his'       : 'his',
    'quarentine': 'quarentine',
    'Quarentine': 'Quarentine',
    'only'      : 'only'
}

# function to convert nltk tag to wordnet tag
def nltk_tag_to_wordnet_tag(nltk_tag):
    if nltk_tag.startswith('J'):
        return wordnet.ADJ
    elif nltk_tag.startswith('V'):
        return wordnet.VERB
    elif nltk_tag.startswith('N'):
        return wordnet.NOUN
    elif nltk_tag.startswith('P'):
        return wordnet.NOUN
    elif nltk_tag.startswith('R'):
        return wordnet.ADV
    else:          
        return None

def lemmatize_sentence(sentence):
    #tokenize the sentence and find the POS tag for each token
    nltk_tagged = nltk.pos_tag(nltk.word_tokenize(sentence))
    # print(nltk_tagged)
    # print(nltk_tagged)
    #tuple of (token, wordnet_tag)
    wordnet_tagged = map(lambda x: (x[0], nltk_tag_to_wordnet_tag(x[1])), nltk_tagged)
    lemmatized_sentence = []
    for word, tag in wordnet_tagged:
        #removing adverb from the final sentense
        if(tag == wordnet.ADV):
            continue
        # # removing adjective from final word
        # if(tag == wornet.ADJ):
        #     continue

        # Excluding words as per the exclude dictonary

        # print(word)
        if word in excptn:
            lemmatized_sentence.append(excptn[word])
            continue
        if word in exclude:
            continue
        word = stemmer.stem(word)
        if tag is None:
            #if there is no available tag, append the token as is
            lemmatized_sentence.append(word)
        else:        
            #else use the tag to lemmatize the token
            lemmatized_sentence.append(lemmatizer.lemmatize(word, tag))
    return " ".join(lemmatized_sentence)

# def word_sense(sentence):
#     word_token = nltk.word_tokenize(sentence)



if __name__ == "__main__":
	if(len(sys.argv)==1):
		print("Format Incorrect, Try - python lemm&stem.py your sentence")
	else:
		s = []
		for x in range(1,len(sys.argv)):
			# print(lemmatize_sentence(sys.argv[x]))
			s.append(lemmatize_sentence(sys.argv[x]))
		print(s)
